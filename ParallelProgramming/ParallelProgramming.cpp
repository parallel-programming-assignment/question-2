// ParallelProgramming.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include "mpi.h"
#include <iostream>
#include <sstream>
#include <time.h>
#include <assert.h>
#include <random>

void Get_data2(
	float*  a_ptr,
	float*  b_ptr,
	int*    n_ptr,
	int     process_rank) {

	if (process_rank == 0) {
		printf("Enter a, b, and n\n");
		printf("a : ");

		float a, b;
		int n;
		std::cin >> a;

		printf("b : ");
		std::cin >> b;

		printf("n : ");
		std::cin >> n;
		
		a_ptr[0] = a;
		b_ptr[0] = b;
		n_ptr[0] = n;

		//std::cout << "a = " << *a_ptr << std::endl;
	}
	MPI_Bcast(a_ptr, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
	MPI_Bcast(b_ptr, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
	MPI_Bcast(n_ptr, 1, MPI_INT, 0, MPI_COMM_WORLD);
} /* Get_data2 */

float Trap(
	float  local_a   /* in */,
	float  local_b   /* in */,
	int    local_n   /* in */,
	float  h         /* in */) {

	float integral;   /* Store result in integral  */
	float x;
	int i;

	float f(float x); /* function we're integrating */

	integral = (f(local_a) + f(local_b)) / 2.0;
	x = local_a;
	for (i = 1; i <= local_n - 1; i++) {
		x = x + h;
		integral = integral + f(x);
	}
	integral = integral * h;
	return integral;
} //  Trap



float f(float x) {
	double return_val;
	return_val = sqrt(pow(2, x) + 1);
	return return_val;
} //function given

int main(int argc, char** argv) {
	int         process_rank;   // My process rank
	int         p;         // processes number
	float       a;         // Left point
	float       b;         // Right point
	int         n;         // Trapezoids numbers
	float       h;         // Trapezoid base length
	float       local_a;   // Left endpoint my process
	float       local_b;   // Right endpoint my process
	int         local_n;   // Number of trapezoids for calculation
	float       integral;  // Integral over my interval
	float       total;     // Total integral
	int         source;    // Process sending integral
	int         dest = 0;  // All messages go to 0
	int         tag = 0;
	MPI_Status  status;

	void Get_data2(float* a_ptr, float* b_ptr, int* n_ptr, int process_rank);
	// Calculate local integral
	float Trap(float local_a, float local_b, int local_n, float h);

	// Let the system do what it needs to start up MPI
	MPI_Init(&argc, &argv);

	// Get my process rank
	MPI_Comm_rank(MPI_COMM_WORLD, &process_rank);

	// Find out how many processes are being used
	MPI_Comm_size(MPI_COMM_WORLD, &p);

	Get_data2(&a, &b, &n, process_rank);

	h = (b - a) / n;    // h is the same for all processes
	local_n = n / p;  // So is the number of trapezoids

	/* Length of each process' interval of
	   integration = local_n*h.  So my interval
	   starts at: */
	local_a = a + process_rank * local_n*h;
	local_b = local_a + local_n * h;
	integral = Trap(local_a, local_b, local_n, h);

	// Add up the integrals calculated by each process
	MPI_Reduce(&integral, &total, 1, MPI_FLOAT,
		MPI_SUM, 0, MPI_COMM_WORLD);


	if (process_rank == 0) {
		printf("When n = %d trapezoids, the estimate area of the integral from %.4f to %.4f  = %f units\n",
			n, a, b, total);
	}

	MPI_Finalize();
}